<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\BillingMaster;
use App\ProjectMaster;
use App\Constant;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:api');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard(){

        $total_client = Client::where('action_type','!=',Constant::ACTION_TYPE_DELETE)->count();
        $total_billings = BillingMaster::where('action_type','!=',Constant::ACTION_TYPE_DELETE)->count();
        $total_projects = ProjectMaster::where('action_type','!=',Constant::ACTION_TYPE_DELETE)->count();
        return response()->json(['total_client'=>$total_client,
                                'total_billings' =>$total_billings,
                                'total_projects' =>$total_projects]);


    }
}
